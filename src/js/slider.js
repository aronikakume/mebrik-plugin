// Global variables
const sliderView = document.querySelector(".mb-slider--view > ul"),
  sliderViewSlides = document.querySelectorAll(".mb-slider--view__slides"),
  arrowLeft = document.querySelector(".mb-slider--arrows__left"),
  arrowRight = document.querySelector(".mb-slider--arrows__right"),
  sliderLength = sliderViewSlides.length;

// Sliding function
const slideMe = (sliderViewItems, isActiveItem) => {
  // remove active class from the current item
  isActiveItem.classList.remove("is-active");
  sliderViewItems.classList.add("is-active");

  // css transform the active slide position4
  sliderView.setAttribute(
    "style",
    `transform: translateX(-${sliderViewItems.offsetLeft}px)`
  );
};

// before slideing function
const beforeSliding = (i) => {
  let isActiveItem = document.querySelector(
      ".mb-slider--view__slides.is-active"
    ),
    currentItem = Array.from(sliderViewSlides).indexOf(isActiveItem) + i,
    nextItem = currentItem + i,
    sliderViewItems = document.querySelector(
      `.mb-slider--view__slides:nth-child(${nextItem})`
    );

  // if nextItem is bigger than the sliderLength, then reset it to 1
  if (nextItem > sliderLength) {
    sliderViewItems = document.querySelector(
      ".mb-slider--view__slides:nth-child(1)"
    );
  }

  // if nextItem is 0
  if (nextItem == 0) {
    sliderViewItems = document.querySelector(
      `.mb-slider--view__slides:nth-child(${sliderLength})`
    );
  }

  // trigger ths sliding method
  slideMe(sliderViewItems, isActiveItem);
};

// triggers arrows
arrowLeft.addEventListener("click", () => beforeSliding(0));
arrowRight.addEventListener("click", () => beforeSliding(1));
