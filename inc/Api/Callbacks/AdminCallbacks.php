<?php

/**
 * @package MebrikPlugin
 */

namespace Inc\Api\Callbacks;

use Inc\Base\BaseController;

class AdminCallbacks extends BaseController
{
  public function adminDashboard()
  {
    return require_once("$this->plugin_path/templates/admin.php");
  }

  public function cptDashboard()
  {
    return require_once("$this->plugin_path/templates/cpt.php");
  }

  public function taxonomyDashboard()
  {
    return require_once("$this->plugin_path/templates/taxonomy.php");
  }

  public function widgetsDashboard()
  {
    return require_once("$this->plugin_path/templates/widgets.php");
  }

  public function mebrikOptionsGroup($input)
  {
    return $input;
  }

  public function mebrikAdminSection()
  {
    echo 'Check this beautiful section here...';
  }

  public function mebrikTextExample()
  {
    $value = esc_attr(get_option('text_example'));
    echo '<input type="text" name="text_example" class="regular-text" value="' . $value . '" placeholder="write something here" />';
  }

  public function mebrikFirstName()
  {
    $value = esc_attr(get_option('first_name'));
    echo '<input type="text" name="first_name" class="regular-text" value="' . $value . '" placeholder="First name" />';
  }
}
