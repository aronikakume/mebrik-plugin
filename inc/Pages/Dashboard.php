<?php

/**
 * @package MebrikPlugin
 */

namespace Inc\Pages;

use \Inc\Api\SettingsApi;
use \Inc\Base\BaseController;
use \Inc\Api\Callbacks\AdminCallbacks;
use \Inc\Api\Callbacks\ManagerCallbacks;

class Dashboard extends BaseController
{

  public $settings;

  public $callbacks;

  public $callbacks_mngr;

  public $pages = array();

  // public $subpages = array();


  public function register()
  {
    $this->settings = new SettingsApi();

    $this->callbacks = new AdminCallbacks();

    $this->callbacks_mngr = new ManagerCallbacks();

    $this->setPages();

    // $this->setSubpages();

    $this->setSettings();

    $this->setSections();

    $this->setFields();

    // threaded function call 
    $this->settings->addPages($this->pages)->withSubPages('dashboard')->register();
  }

  // create menu page
  public function setPages()
  {
    $this->pages = array(
      array(
        'page_title' => 'Mebrik Plugin',
        'menu_title' => 'Mebrik',
        'capability' => 'manage_options',
        'menu_slug' => 'mebrik_plugin',
        'callback' => array($this->callbacks, 'adminDashboard'), //callback function
        'icon_url' => 'dashicons-store',
        'position' => 110
      )
    );
  }

  // Create subpages
  // public function setSubpages()
  // {
  //   $this->subpages = array(
  //     array(
  //       'parent_slug ' => 'mebrik_plugin',
  //       'page_title' => 'Custom Post Types',
  //       'menu_title' => 'CPT',
  //       'capability' => 'manage_options',
  //       'menu_slug' => 'mebrik_cpt',
  //       'callback' => array($this->callbacks, 'cptDashboard') //callback function
  //     ),
  //     array(
  //       'parent_slug ' => 'mebrik_plugin',
  //       'page_title' => 'Custom Taxonomies',
  //       'menu_title' => 'Taxonomies',
  //       'capability' => 'manage_options',
  //       'menu_slug' => 'mebrik_taxonomies',
  //       'callback' => array($this->callbacks, 'taxonomyDashboard') //callback function
  //     ),
  //     array(
  //       'parent_slug ' => 'mebrik_plugin',
  //       'page_title' => 'Custom Widgets',
  //       'menu_title' => 'Widgets',
  //       'capability' => 'manage_options',
  //       'menu_slug' => 'mebrik_widgets',
  //       'callback' => array($this->callbacks, 'widgetsDashboard') //callback function
  //     ),
  //   );
  // }

  public function setSettings()
  {

    $args = array(
      array(
        'option_group' => 'mebrik_plugin_settings',
        'option_name' => 'mebrik_plugin',
        'callback' => array($this->callbacks_mngr, 'checkboxSanitize')
      )
    );

    $this->settings->setSettings($args);
  }

  public function setSections()
  {
    $args = [
      [
        'id' => 'mebrik_admin_index',
        'title' => 'Settings Manager',
        'callback' => array($this->callbacks_mngr, 'AdminSectionManager'),
        'page' => 'mebrik_plugin'
      ]
    ];

    $this->settings->setSections($args);
  }

  public function setFields()
  {

    $args = array();

    foreach ($this->managers as $key => $value) {
      $args[] = array(
        'id' => $key,
        'title' => $value,
        'callback' => array($this->callbacks_mngr, 'checkboxField'),
        'page' => 'mebrik_plugin',
        'section' => 'mebrik_admin_index',
        'args' => array(
          'option_name' => 'mebrik_plugin',
          'label_for' => $key,
          'class' => 'ui-toggle'
        )
      );
    }

    $this->settings->setFields($args);
  }
}
