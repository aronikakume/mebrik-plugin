<?php

/**
 * @package MebrikPlugin
 */

namespace Inc\Base;

use \Inc\Base\BaseController;

class Enqueue extends BaseController
{
  public function register()
  {
    add_action('admin_enqueue_scripts', array($this, 'enqueue'));
  }

  function enqueue()
  {

    wp_enqueue_script( 'media-upload' );
    wp_enqueue_media();
    wp_enqueue_style('mebrik-style',  $this->plugin_url . 'assets/style.css');
    wp_enqueue_script('mebrik-script', $this->plugin_url . 'assets/mebrikScript.js');
  }
}
