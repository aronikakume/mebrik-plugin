<?php

/**
 * @package MebrikPlugin
 */

namespace Inc\Base;

use \Inc\Api\SettingsApi;
use \Inc\Base\BaseController;
use \Inc\Api\Callbacks\AdminCallbacks;
use \Inc\Api\Callbacks\TaxonomyCallbacks;

class CustomTaxonomyController extends BaseController
{

    public $settings;

    public $callbacks;

    public $tax_callbacks;

    public $subpages = array();

    public $taxonomies = array();


    public function register()
    {

        if (!$this->activated('taxonomy_manager')) return;

        $this->settings = new SettingsApi();

        $this->callbacks = new AdminCallbacks();

        $this->tax_callbacks = new TaxonomyCallbacks();

        $this->setSubpages();

        $this->setSettings();

        $this->setSections();

        $this->setFields();

        $this->settings->addSubPages($this->subpages)->register();

        $this->storeCustomTaxonomies();

        if (!empty($this->taxonomies)) {
            add_action('init', array($this, 'registerCustomTaxonomy'));
        }
    }

    public function setSubpages()
    {
        $this->subpages = array(
            array(
                'parent_slug' => 'mebrik_plugin',
                'page_title' => 'Taxonomy Manager',
                'menu_title' => 'Taxonomy Manager',
                'capability' => 'manage_options',
                'menu_slug' => 'mebrik_taxonomy',
                'callback' => array($this->callbacks, 'taxonomyDashboard'),
            )
        );
    }

    public function setSettings()
    {
        $args = array(
            array(
                'option_group' => 'mebrik_plugin_tax_settings',
                'option_name' => 'mebrik_plugin_tax',
                'callback' => array($this->tax_callbacks, 'taxSanitize')
            )
        );

        $this->settings->setSettings($args);
    }

    public function setSections()
    {
        $args = [
            [
                'id' => 'mebrik_tax_index',
                'title' => 'Custom Taxonomy Manager',
                'callback' => array($this->tax_callbacks, 'taxSectionManager'),
                'page' => 'mebrik_taxonomy'
            ]
        ];

        $this->settings->setSections($args);
    }

    public function setFields()
    {
        $args = array(
            array(
                'id' => 'taxonomy',
                'title' => 'Custom Taxonomy ID',
                'callback' => array($this->tax_callbacks, 'textField'),
                'page' => 'mebrik_taxonomy',
                'section' => 'mebrik_tax_index',
                'args' => array(
                    'option_name' => 'mebrik_plugin_tax',
                    'label_for' => 'taxonomy',
                    'placeholder' => 'eg. genre',
                    'array' => 'taxonomy'
                )
            ),
            array(
                'id' => 'singular_name',
                'title' => 'Singular Name',
                'callback' => array($this->tax_callbacks, 'textField'),
                'page' => 'mebrik_taxonomy',
                'section' => 'mebrik_tax_index',
                'args' => array(
                    'option_name' => 'mebrik_plugin_tax',
                    'label_for' => 'singular_name',
                    'placeholder' => 'eg. Genre',
                    'array' => 'taxonomy'
                )
            ),
            array(
                'id' => 'hierarchical',
                'title' => 'Hierarchical',
                'callback' => array($this->tax_callbacks, 'checkboxField'),
                'page' => 'mebrik_taxonomy',
                'section' => 'mebrik_tax_index',
                'args' => array(
                    'option_name' => 'mebrik_plugin_tax',
                    'label_for' => 'hierarchical',
                    'class' => 'ui-toggle',
                    'array' => 'taxonomy'
                )
            ),
            array(
                'id' => 'objects',
                'title' => 'Post Types',
                'callback' => array($this->tax_callbacks, 'checkboxPostTypeField'),
                'page' => 'mebrik_taxonomy',
                'section' => 'mebrik_tax_index',
                'args' => array(
                    'option_name' => 'mebrik_plugin_tax',
                    'label_for' => 'objects',
                    'class' => 'ui-toggle',
                    'array' => 'taxonomy'
                )
            ),

        );
        $this->settings->setFields($args);
    }

    public function storeCustomTaxonomies()
    {
        if (!get_option('mebrik_plugin_tax')) {
            return;
        }

        $options = get_option('mebrik_plugin_tax');
        foreach ($options as $option) {
            $labels = array(
                'name'                       => $option['singular_name'],
                'singular_name'              => $option['singular_name'],
                'menu_name'                  => $option['singular_name'],
                'all_items'                  => __('All ' . $option['singular_name'], 'text_domain'),
                'parent_item'                => __('Parent ' . $option['singular_name'], 'text_domain'),
                'parent_item_colon'          => __('Parent ' . $option['singular_name'] . ':', 'text_domain'),
                'new_item_name'              => __('New ' . $option['singular_name'] . ' Name', 'text_domain'),
                'add_new_item'               => __('Add New ' . $option['singular_name'], 'text_domain'),
                'edit_item'                  => __('Edit ' . $option['singular_name'], 'text_domain'),
                'update_item'                => __('Update ' . $option['singular_name'], 'text_domain'),
                'view_item'                  => __('View ' . $option['singular_name'], 'text_domain'),
                'separate_items_with_commas' => __('Separate ' . $option['singular_name'] . ' with commas', 'text_domain'),
                'add_or_remove_items'        => __('Add or remove ' . $option['singular_name'], 'text_domain'),
                'choose_from_most_used'      => __('Choose from the most used', 'text_domain'),
                'popular_items'              => __('Popular ' . $option['singular_name'], 'text_domain'),
                'search_items'               => __('Search ' . $option['singular_name'], 'text_domain'),
                'not_found'                  => __('Not Found', 'text_domain'),
                'no_terms'                   => __('No items', 'text_domain'),
                'items_list'                 => __($option['singular_name'] . 's list', 'text_domain'),
                'items_list_navigation'      => __($option['singular_name'] . 's list navigation', 'text_domain'),
            );
            $this->taxonomies[] = array(
                'labels'                     => $labels,
                'hierarchical'               =>  isset($option['hierarchical']) ? true : false,
                'public'                     => true,
                'show_ui'                    => true,
                'show_admin_column'          => true,
                'show_in_nav_menus'          => true,
                'show_tagcloud'              => true,
                'rewrite'                    => array('slug' => $option['taxonomy']),
                'objects'                    => isset($option['objects']) ? $option['objects']: null
            );
        }
    }

    // Register the custom taxonomy
    public function registerCustomTaxonomy()
    {
        foreach ($this->taxonomies as $taxonomy) {
            $objects = isset($taxonomy['objects'])? array_keys($taxonomy['objects']): null;
            register_taxonomy($taxonomy['rewrite']['slug'], $objects, $taxonomy);
        }
    }
}
