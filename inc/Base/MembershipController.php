<?php

/**
 * @package MebrikPlugin
 */

namespace Inc\Base;

use \Inc\Api\SettingsApi;
use \Inc\Base\BaseController;
use \Inc\Api\Callbacks\AdminCallbacks;

class MembershipController extends BaseController
{
    public $subpages = array();

    public $settings;

    public $callbacks;

    public function register()
    {

        if (!$this->activated('membership_manager')) return;

        $this->settings = new SettingsApi();

        $this->callbacks = new AdminCallbacks();

        $this->setSubpages();

        $this->settings->addSubPages($this->subpages)->register();

        // add_action('init', array($this, 'activate'));
    }

    public function setSubpages()
    {
        $this->subpages = array(
            array(
                'parent_slug' => 'mebrik_plugin',
                'page_title' => 'Membership Manager',
                'menu_title' => 'Membership Manager',
                'capability' => 'manage_options',
                'menu_slug' => 'mebrik_membershipmanager',
                'callback' => array($this->callbacks, 'cptDashboard'),
            )
        );
    }
}
