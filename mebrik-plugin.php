<?php

/**
 * @package MebrikPlugin
 */


/*
  Plugin Name: Mebrik Plugin
  Plugin URI: http://mebrik.com/plugin
  Description: This is my first attempt on writing a custom plugin for this amazing tutorial series.
  Version: 1.0.0
  Author: Mebratu Kumera
  Autour URI: http://mebrik.com
  License: GPLv2 or later
  Text Domain: mebrik-plugin
*/

/*
This program is free software: you can redistribute
 it and/or modify it under the terms of the GNU General 
 Public License as published by the Free Software Foundation,
  either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the GNU General Public License for more details.

You should have received a copy of the GNU General
 Public License along with this program. If not,
  see <https://www.gnu.org/licenses/>.
*/

// If this file is called firectly, then abort
defined('ABSPATH') or die('Hey, you can\'t access this file, you silly human!');

// Require once the composer autoload
if (file_exists(dirname(__FILE__) . '/vendor/autoload.php')) {
  require_once dirname(__FILE__) . '/vendor/autoload.php';
}



use Inc\Base\Activate;
use Inc\Base\Deactivate;


/*
*
* The code that  runs during the plugin activation
*
*/

function activate_mebrik_plugin()
{
  Activate::activate();
}

/*
*
* The code that  runs during the plugin activation
*
*/

function deactivate_mebrik_plugin()
{
  Deactivate::deactivate();
}


register_activation_hook(__FILE__, 'activate_mebrik_plugin');
register_deactivation_hook(__FILE__, 'deactivate_mebrik_plugin');

/*
* 
* Initialize all the core class of the plugin
*
*/

if (class_exists('Inc\\Init')) {
  Inc\Init::register_services();
}
