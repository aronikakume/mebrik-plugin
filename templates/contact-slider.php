<?php

$args = array(
    'post_type' => 'testimonial',
    'posts_per_page' => 5,
    'post_status' => 'publish',
    'meta_query' => array(
        array(
            'key' => '_mebrik_testimonial_key',
            'value' => 's:8:"approved";i:1;s:8:"featured";i:1;',
            'compare' => 'LIKE'
        )
    )
);

$testimonials = new WP_Query($args);

if ($testimonials->have_posts()) : ?>
    <?php $i = 1; ?>
    <div class="mb-slider--wrapper">
        <div class="mb-slider--container">
            <div class="mb-slider--view">
                <ul>
                    <?php while ($testimonials->have_posts()) : $testimonials->the_post(); ?>
                        <?php
                        $name = get_post_meta(get_the_ID(), '_mebrik_testimonial_key', true)['name'] ?? '';
                        ?>
                        <li class="mb-slider--view__slides<?php echo $i === 1 ? ' is-active' : ''; ?>">
                            <div class="testimonial-quote">
                                <?php the_content(); ?>
                            </div>
                            <div class="testimonial-author">
                                ~ <?php echo $name; ?> ~
                            </div>
                        </li>
                        <?php $i++; ?>
                    <?php endwhile; ?>
                </ul>
            </div>
            <div class="mb-slider--arrows">
                <span class="arrow mb-slider--arrows__left">
                    &#x3c;
                </span>
                <span class="arrow mb-slider--arrows__right">
                    &#x3e;
                </span>
            </div>
        </div>
    </div>
<?php endif;

wp_reset_postdata();
