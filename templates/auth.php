<form action="#" id="mebrik-auth-form" method="post" data-url="<?php echo admin_url('admin-ajax.php'); ?>">
    <div class="auth-btn">
        <input type="button" value="Login" id="mebrik-show-auth-form">
    </div>

    <div id="mebrik-auth-container" class="auth-container">
        <a href="#" id="mebrik-auth-close" class="close">&times;</a>
        <h2>Site Login</h2>
        <label for="username">Username</label>
        <input type="text" id="username" name="username">
        <label for="password">Password</label>
        <input type="password" id="password" name="password">
        <input type="submit" class="submit_button" value="Login" name="submit">
        <p class="status" data-message="status"></p>

        <p class="actions">
            <a href="<?php echo wp_lostpassword_url(); ?>">Forgot Password?</a>
            - <a href="<?php echo wp_registration_url(); ?>">Register</a>
        </p>
        <input type="hidden" name="action" value="mebrik_login">
        <?php wp_nonce_field('ajax-login-nonce', 'mebrik_auth'); ?>
    </div>
</form>